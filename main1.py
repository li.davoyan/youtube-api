from flask import Flask, request, jsonify, abort, make_response
from flask_sqlalchemy import SQLAlchemy
from uuid import uuid4
import re

# Инициализация Flask приложения
app = Flask(__name__)
# Настройка подключения к базе данных
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///playlists.db'
# Инициализация объекта SQLAlchemy
db = SQLAlchemy(app)

# Определение модели Playlist для базы данных
class Playlist(db.Model):
    id = db.Column(db.String(36), primary_key=True)  # Уникальный идентификатор плейлиста
    name = db.Column(db.String(64), nullable=False)  # Название плейлиста
    description = db.Column(db.String(256))  # Описание плейлиста

    def __init__(self, name, description=None):
        self.id = str(uuid4())
        self.name = name
        self.description = description

# Определение модели Video для базы данных
class Video(db.Model):
    id = db.Column(db.String(36), primary_key=True)  # Уникальный идентификатор видео
    youtube_id = db.Column(db.String(11), nullable=False)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(256))
    thumbnail_url = db.Column(db.String(256))
    playlist_id = db.Column(db.String(36), db.ForeignKey('playlist.id'), nullable=False)

    def __init__(self, youtube_id, title, description=None, thumbnail_url=None, playlist_id=None):
        self.id = str(uuid4())  # Генерация уникального ID
        self.youtube_id = youtube_id
        self.title = title
        self.description = description
        self.thumbnail_url = thumbnail_url
        self.playlist_id = playlist_id

# Обработчик ошибок для неверных запросов
@app.errorhandler(400)
def handle_bad_request(e):
    return make_response(jsonify({'error': 'Bad request', 'message': str(e)}), 400)

# Маршрут для создания нового плейлиста
@app.route('/playlists', methods=['POST'])
def create_playlist():
    data = request.get_json()
    name = data.get('name')
    description = data.get('description', '')

    # Проверка корректности имени плейлиста
    if not name or not re.match(r'^[a-zA-Zа-яА-ЯёЁ\s]{1,64}$', name):
        abort(400, 'Invalid name')

    playlist = Playlist(name=name, description=description)
    db.session.add(playlist)
    db.session.commit()

    # Ответ сервера после создания плейлиста
    return jsonify({
        'message': 'Плейлист успешно создан',
        'id': playlist.id,
        'name': playlist.name,
        'description': playlist.description
    }), 201

# Маршрут для получения списка всех плейлистов
@app.route('/playlists', methods=['GET'])
def list_playlists():
    playlists = Playlist.query.all()
    # Формирование JSON-ответа со списком плейлистов
    return jsonify([{'id': pl.id, 'name': pl.name, 'description': pl.description} for pl in playlists])

# Маршрут для обновления информации о плейлисте
@app.route('/playlists/<playlist_id>', methods=['PUT'])
def update_playlist(playlist_id):
    playlist = Playlist.query.get_or_404(playlist_id)
    data = request.get_json()
    name = data.get('name')
    description = data.get('description', playlist.description)

    # Проверка корректности имени плейлиста
    if name and not re.match(r'^[a-zA-Zа-яА-ЯёЁ\s]{1,64}$', name):
        abort(400, 'Invalid name')

    playlist.name = name
    playlist.description = description
    db.session.commit()

    # Ответ сервера после обновления плейлиста
    return jsonify({'id': playlist.id, 'name': playlist.name, 'description': playlist.description})

# Маршрут для удаления плейлиста
@app.route('/playlists/<playlist_id>', methods=['DELETE'])
def delete_playlist(playlist_id):
    playlist = Playlist.query.get_or_404(playlist_id)
    db.session.delete(playlist)
    db.session.commit()
    # Ответ сервера после удаления плейлиста
    return jsonify({'message': 'Playlist deleted'})

# Маршрут для добавления видео в плейлист
@app.route('/playlists/<playlist_id>/videos', methods=['POST'])
def add_video_to_playlist(playlist_id):
    data = request.get_json()
    youtube_id = data.get('youtube_id')
    title = data.get('title')
    description = data.get('description', '')
    thumbnail_url = data.get('thumbnail_url', '')

    # Проверка наличия необходимых данных
    if not youtube_id or not title:
        abort(400, 'Missing youtube_id or title')

    video = Video(youtube_id=youtube_id, title=title, description=description, thumbnail_url=thumbnail_url, playlist_id=playlist_id)
    db.session.add(video)
    db.session.commit()

    # Ответ сервера после добавления видео
    return jsonify({'id': video.id, 'youtube_id': video.youtube_id, 'title': video.title, 'description': video.description, 'thumbnail_url': video.thumbnail_url}), 201

# Маршрут для получения списка видео в плейлисте
@app.route('/playlists/<playlist_id>/videos', methods=['GET'])
def list_videos_in_playlist(playlist_id):
    videos = Video.query.filter_by(playlist_id=playlist_id).all()
    # Формирование JSON-ответа со списком видео
    return jsonify([{'id': video.id, 'youtube_id': video.youtube_id, 'title': video.title, 'description': video.description, 'thumbnail_url': video.thumbnail_url} for video in videos])

# Маршрут для удаления видео из плейлиста
@app.route('/playlists/<playlist_id>/videos/<video_id>', methods=['DELETE'])
def remove_video_from_playlist(playlist_id, video_id):
    video = Video.query.filter_by(playlist_id=playlist_id, id=video_id).first_or_404()
    db.session.delete(video)
    db.session.commit()
    # Ответ сервера после удаления видео
    return jsonify({'message': 'Video removed'})

# Точка входа для запуска приложения
if __name__ == '__main__':
    with app.app_context():
        db.create_all()  # Создание всех таблиц в базе данных
    app.run(debug=True)  # Запуск приложения в режиме отладки