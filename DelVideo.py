import requests

def delete_video_from_playlist(playlist_id, youtube_id):
    url = f'http://localhost:5000/playlists/{playlist_id}/videos/{youtube_id}'
    response = requests.delete(url)
    if response.status_code == 200:
        print("Видео успешно удалено")
        return True
    else:
        print(f"Ошибка при удалении видео: {response.status_code}, {response.text}")
        return False

# ID видео на YouTube и плейлиста, из которого нужно удалить видео
youtube_id = '28eacc41-72d5-4e9d-8e35-6408b2d25e73'
playlist_id = '1ccfdc42-66a4-4215-b9bc-abb45bd481f1'

# Вызов функции для удаления видео из плейлиста
is_deleted = delete_video_from_playlist(playlist_id, youtube_id)
print(f'Видео удалено: {is_deleted}')