import unittest
import requests

class TestPlaylistAPI(unittest.TestCase):
    def setUp(self):
        self.base_url = 'http://localhost:5000/playlists'

    def test_update_playlist(self):
        # ID плейлиста, который нужно обновить
        playlist_id = '1ccfdc42-66a4-4215-b9bc-abb45bd481f1'
        update_url = f'{self.base_url}/{playlist_id}'

        # Новые данные для плейлиста
        updated_data = {
            'name': 'Новое имя плейлиста',
            'description': 'Новое описание плейлиста'
        }

        # Отправляем PUT-запрос для обновления плейлиста
        response = requests.put(update_url, json=updated_data)

        # Проверяем статус-код ответа
        if response.status_code == 200:
            # Получаем обновлённый плейлист и проверяем изменения
            updated_playlist = response.json()
            self.assertEqual(updated_playlist['name'], updated_data['name'])
            self.assertEqual(updated_playlist['description'], updated_data['description'])
            print('Плейлист успешно изменён.')
        else:
            self.fail(f'Ошибка при изменении плейлиста: {response.status_code}')

if __name__ == '__main__':
    unittest.main()