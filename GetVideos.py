import requests

def list_videos_in_playlist(playlist_id):
    url = f'http://localhost:5000/playlists/{playlist_id}/videos'
    response = requests.get(url)
    if response.status_code == 200:
        videos = response.json()
        for video in videos:
            print(f"ID видео: {video['id']}, Название: {video['title']}, Описание: {video['description']}")
        return videos
    else:
        print(f"Ошибка при получении списка видео: {response.status_code}, {response.text}")
        return None

def test_pagination_of_playlist_videos(playlist_id, page, per_page):
    url = f'http://localhost:5000/playlists/{playlist_id}/videos?page={page}&per_page={per_page}'
    response = requests.get(url)
    if response.status_code == 200:
        videos = response.json()
        if len(videos) <= per_page:
            print(f"Тест пагинации успешен. Получено видео: {len(videos)}")
            return True
        else:
            print("Ошибка пагинации: количество видео превышает per_page")
            return False
    else:
        print(f"Ошибка при получении списка видео: {response.status_code}, {response.text}")
        return False

# ID плейлиста для тестирования
playlist_id = '1ccfdc42-66a4-4215-b9bc-abb45bd481f1'

# Вызов функции для вывода списка видео в плейлисте
videos = list_videos_in_playlist(playlist_id)

# Параметры пагинации
page_number = 1
videos_per_page = 10

# Вызов функции для тестирования пагинации
is_pagination_correct = test_pagination_of_playlist_videos(playlist_id, page_number, videos_per_page)
print(f'Пагинация работает корректно: {is_pagination_correct}')