import unittest
import requests

class TestPlaylistAPI(unittest.TestCase):
    # URL сервиса для добавления видео в плейлист
    BASE_URL = 'http://localhost:5000/playlists'

    def test_add_video_to_playlist(self):
        playlist_id = '1ccfdc42-66a4-4215-b9bc-abb45bd481f1'
        youtube_id = 'oGg8Gi0T7Rs'
        url = f'{self.BASE_URL}/{playlist_id}/videos'
        video_data = {
            'title': 'Заголовок вашего видео',
            'description': 'Описание вашего видео',
            'youtube_id': youtube_id
        }
        response = requests.post(url, json=video_data)
        self.assertEqual(response.status_code, 201, f"Ошибка при добавлении видео: {response.status_code}, {response.text}")

if __name__ == '__main__':
    unittest.main()