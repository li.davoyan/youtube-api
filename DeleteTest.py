import unittest
import requests

class TestPlaylistAPI(unittest.TestCase):
    def setUp(self):
        self.base_url = 'http://localhost:5000/playlists'


    def test_delete_playlist(self):
        # ID плейлиста, который нужно удалить
        playlist_id = '0b82001d-23f9-4fee-9b84-a9d02587a082'
        delete_url = f'{self.base_url}/{playlist_id}'

        # Отправляем DELETE-запрос для удаления плейлиста
        response = requests.delete(delete_url)

        # Проверяем статус-код ответа и выводим соответствующее сообщение
        if response.status_code == 200:
            print('Плейлист успешно удалён.')
        else:
            print('Ошибка: Не удалось удалить плейлист.')

        # Проверяем, что плейлист больше не перечисляется в списке
        response = requests.get(self.base_url)
        playlists = response.json()
        self.assertNotIn(playlist_id, [playlist['id'] for playlist in playlists])

if __name__ == '__main__':
    unittest.main()