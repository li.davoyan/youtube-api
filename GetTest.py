import unittest
import requests


class TestPlaylistAPI(unittest.TestCase):
    def setUp(self):
        self.base_url = 'http://localhost:5000/playlists'

    def test_list_playlists(self):
        # Отправляем GET-запрос для получения списка плейлистов
        response = requests.get(self.base_url)
        self.assertEqual(response.status_code, 200)

        # Проверяем, что ответ содержит список плейлистов
        playlists = response.json()
        self.assertIsInstance(playlists, list)

        # Проверяем, что каждый плейлист содержит необходимые ключи
        for playlist in playlists:
            self.assertIn('id', playlist)
            self.assertIn('name', playlist)
            self.assertIn('description', playlist)

        # Выводим список плейлистов
        print('Список плейлистов:')
        for playlist in playlists:
            print(f"ID: {playlist['id']}, Имя: {playlist['name']}, Описание: {playlist['description']}")


if __name__ == '__main__':
    unittest.main()