import unittest
import requests

class TestPlaylistAPI(unittest.TestCase):
    def setUp(self):
        self.base_url = 'http://localhost:5000/playlists'

    def test_create_playlist_with_valid_data(self):
        # Отправляем POST-запрос для создания плейлиста с валидными данными
        valid_data = {'name': 'Спортивная музыка', 'description': 'Лучшие треки для спорта'}
        response = requests.post(self.base_url, json=valid_data)
        self.assertEqual(response.status_code, 201)

        # Проверяем, что ответ содержит сообщение о успешном создании
        data = response.json()
        self.assertEqual(data['message'], 'Плейлист успешно создан')
        self.assertIn('id', data)
        self.assertEqual(data['name'], valid_data['name'])
        self.assertEqual(data['description'], valid_data['description'])

    def test_create_playlist_with_invalid_data(self):
        # Отправляем POST-запрос для создания плейлиста с невалидными данными
        invalid_data = {'name': '', 'description': 'Описание'}
        response = requests.post(self.base_url, json=invalid_data)
        self.assertEqual(response.status_code, 400)

        # Проверяем, что ответ содержит сообщение об ошибке
        data = response.json()
        self.assertIn('error', data)
        self.assertEqual(data['error'], 'Bad request')

if __name__ == '__main__':
    unittest.main()